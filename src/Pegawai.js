import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Navigate } from "react-router-dom";
import "./Popup.css";

const Pegawai = () => {
  const [employees, setEmployees] = useState([]);
  const [showAddPopup, setShowAddPopup] = useState(false);
  const [showEditPopup, setShowEditPopup] = useState(false);
  const [editedEmployee, setEditedEmployee] = useState(null);
  const [id, setId] = useState(null);
  const [name, setName] = useState('');
  const [address, setAddress] = useState('');
  const [picture, setPicture] = useState(null);
  const [showConfirmation, setShowConfirmation] = useState(false);
  const [employeeToDelete, setEmployeeToDelete] = useState(null);

  useEffect(() => {
    fetchEmployees();
  }, []);

  const fetchEmployees = () => {
    axios.get('http://localhost:8080/pegawai')
      .then((response) => {
        console.log(localStorage.getItem("authenticated"));
        if (response.status === 200) {
          setEmployees(response.data.data);
        } else {
          console.log('Failed to fetch employees');
        }
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  };

  const handleEdit = (employee) => {
    setId(employee.id);
    setName(employee.nama_pegawai);
    setAddress(employee.alamat_pegawai);
    setPicture(null); // Clear the picture value for editing
    console.log(employee);
    setEditedEmployee(employee);
    setShowEditPopup(true);
  };

  const handleDelete = (employee) => {
    setEmployeeToDelete(employee);
    setShowConfirmation(true);
  };

  const confirmDelete = () => {
    if (employeeToDelete) {
      axios.delete(`http://localhost:8080/pegawai/${employeeToDelete.id}`)
        .then((response) => {
          if (response.status === 200) {
            console.log('Employee deleted successfully');
            fetchEmployees();
            closeConfirmation();
          } else {
            console.log('Failed to delete employee');
          }
        })
        .catch((error) => {
          console.error('Error:', error);
        });
    }
  };

  const closeConfirmation = () => {
    setShowConfirmation(false);
    setEmployeeToDelete(null);
  };

  const handleAddEmployee = () => {
    setShowAddPopup(true);
  };

  const handlePopupClose = () => {
    setShowAddPopup(false);
    setShowEditPopup(false);
    setEditedEmployee(null);
    setName('');
    setAddress('');
    setPicture(null);
  };

  const handleIdChange = (e) => {
    setId(e.target.value);
  };

  const handleNameChange = (e) => {
    setName(e.target.value);
  };

  const handleAddressChange = (e) => {
    setAddress(e.target.value);
  };

  const handlePictureChange = (e) => {
    setPicture(e.target.files[0]);
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();

    const formData = new FormData();
    formData.append('nama', name);
    formData.append('alamat', address);
    formData.append('file', picture);

    axios.post('http://localhost:8080/pegawai', formData)
      .then((response) => {
        if (response.status === 200) {
          console.log('Employee added successfully');
          fetchEmployees();
          handlePopupClose();
        } else {
          console.log('Failed to add employee');
        }
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  };

  const handleFormUpdate = (e) => {
    e.preventDefault();

    const formData = new FormData();
    formData.append('nama', name);
    formData.append('alamat', address);
    formData.append('file', picture);

    axios.post('http://localhost:8080/pegawai/'+id, formData)
      .then((response) => {
        if (response.status === 200) {
          console.log('Employee edited successfully');
          fetchEmployees();
          handlePopupClose();
        } else {
          console.log('Failed to add employee');
        }
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  };

  if (!localStorage.getItem("authenticated")) {
    return <Navigate replace to="/login" />;
  } else {
    return (
      <div className="container">
        <h2 className="text-center mt-4">Employee List</h2>
        <div className="text-end mb-3">
          <button className="btn btn-primary" onClick={handleAddEmployee}>Add New Employee</button>
        </div>
        <table className="table table-bordered">
          <thead>
            <tr>
              <th>Name</th>
              <th>Address</th>
              <th>Picture</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {employees.map((employee) => (
              <tr key={employee.id}>
                <td>{employee.nama_pegawai}</td>
                <td>{employee.alamat_pegawai}</td>
                <td>
                  <img src={employee.foto_pegawai} alt="Profile" className="img-fluid" style={{ maxWidth: '100px' }} />
                </td>
                <td>
                  <button className="btn btn-primary btn-sm" onClick={() => handleEdit(employee)}>Edit</button>
                  <button className="btn btn-danger btn-sm ms-2" onClick={() => handleDelete(employee)}>Delete</button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        {showAddPopup && (
        <div className="popup">
          <div className="popup-content">
            <h3>Add Employee</h3>
            <form onSubmit={handleFormSubmit}>
              <div className="mb-3">
                <label htmlFor="name" className="form-label">Name:</label>
                <input type="text" id="name" className="form-control" value={name} onChange={handleNameChange} required />
              </div>
              <div className="mb-3">
                <label htmlFor="address" className="form-label">Address:</label>
                <input type="text" id="address" className="form-control" value={address} onChange={handleAddressChange} required />
              </div>
              <div className="mb-3">
                <label htmlFor="picture" className="form-label">Picture:</label>
                <input type="file" id="picture" className="form-control" onChange={handlePictureChange} required />
              </div>
              <div className="text-center">
                <button type="submit" className="btn btn-primary">Submit</button>
                <button type="button" className="btn btn-secondary ms-2" onClick={handlePopupClose}>Cancel</button>
              </div>
            </form>
          </div>
        </div>
      )}
      {showEditPopup && editedEmployee && (
        <div className="popup">
          {/* Edit Employee Popup */}
          <div className="popup-content">
            <h3>Edit Employee</h3>
            <form onSubmit={handleFormUpdate}>
              <div className="mb-3">
                <label htmlFor="name" className="form-label">Name:</label>
                <input type="text" id="name" className="form-control" value={name} onChange={handleNameChange} required />
                <input type="hidden" id="id" className="form-control" value={id} onChange={handleIdChange} />
              </div>
              <div className="mb-3">
                <label htmlFor="address" className="form-label">Address:</label>
                <input type="text" id="address" className="form-control" value={address} onChange={handleAddressChange} required />
              </div>
              <div className="mb-3">
                <label htmlFor="picture" className="form-label">Picture:</label>
                <input type="file" id="picture" className="form-control" onChange={handlePictureChange} />
              </div>
              <div className="text-center">
                <button type="submit" className="btn btn-primary">Submit</button>
                <button type="button" className="btn btn-secondary ms-2" onClick={handlePopupClose}>Cancel</button>
              </div>
            </form>
          </div>
        </div>
      )}
      {showConfirmation && (
        <div className="confirmation">
          <div className="confirmation-content">
            <h3>Confirm Delete</h3>
            <p>Are you sure you want to delete this employee?</p>
            <div className="text-center">
              <button className="btn btn-danger" onClick={confirmDelete}>Delete</button>
              <button className="btn btn-secondary ms-2" onClick={closeConfirmation}>Cancel</button>
            </div>
          </div>
        </div>
      )}
      </div>
    );
  }
};

export default Pegawai;
