import { useEffect, useState } from "react";
import { Navigate, Link } from "react-router-dom";

const Dashboard = ({ token }) => {
  const [authenticated, setauthenticated] = useState(null);
  console.log(authenticated);
  useEffect(() => {
    const loggedInUser = localStorage.getItem("authenticated");
    console.log(loggedInUser);
    if (loggedInUser) {
      setauthenticated(true);
    }
    console.log(authenticated);
  }, []);

  if (!localStorage.getItem("authenticated")) {
    return <Navigate replace to="/login" />;
  } else {
    return (
      <div className="container">
        <h2 className="text-center mt-4">Welcome</h2>
        <div className="row justify-content-center">
          <div className="col-md-6 text-center">
            <Link to="/userinfo" className="btn btn-info btn-lg">Users</Link>
          </div>
          <div className="col-md-6 text-center">
            <Link to="/pegawai" className="btn btn-primary btn-lg">Pegawai</Link>
          </div>
        </div>
      </div>
    );
  }
};

export default Dashboard;