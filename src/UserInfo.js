import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Navigate } from "react-router-dom";

const UserInfo = () => {
  const [name, setName] = useState('');
  const [username, setUsername] = useState('');

  useEffect(() => {
    // Fetch user info when the component mounts
    fetchUserInfo();
  }, []);

  const fetchUserInfo = () => {
    // Make a GET request to the user info endpoint
    const token = localStorage.getItem('token');

    const config = {
      headers: {
        Authorization: `Bearer ${token}` // Include the bearer token in the Authorization header
      }
    };

    // Make a GET request to the user info endpoint with the token
    axios.get('http://localhost:8080/me', config)
    .then((response) => {
      if (response.status === 200) {
        const data = response.data.data;
        console.log(data);
        setName(data.nama);
        setUsername(data.username);
      } else {
        console.log('Failed to fetch user info');
      }
    })
    .catch((error) => {
      console.error('Error:', error);
    });
  };

  if (!localStorage.getItem("authenticated")) {
    return <Navigate replace to="/login" />;
  } else {
    return (
      <div className="container">
        <div className="row justify-content-center mt-4">
          <div className="col-lg-4 col-md-6">
            <h2 className="text-center mb-4">User Info</h2>
            <div className="text-center">
              <h4>Nama : {name}</h4>
              <h5>Username : {username}</h5>
            </div>
          </div>
        </div>
      </div>
    );
  }
};

export default UserInfo;
