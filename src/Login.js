import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from "react-router-dom";

const Login = () => {
  const navigate = useNavigate();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [authenticated, setauthenticated] = useState(localStorage.getItem(localStorage.getItem("authenticated")|| false));

  const handleUsernameChange = (e) => {
    setUsername(e.target.value);
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const userData = {
      username,
      password
    };

    // Make a POST request to the login endpoint
    axios.post('http://localhost:8080/login', userData)
      .then((response) => {
        console.log(response.data.token);
        // Handle the login response
        if (response.status === 200) {
          setauthenticated(true)
          localStorage.setItem("authenticated", true);
          localStorage.setItem("token", response.data.token);
          const token = response.data.token;
          navigate('/dashboard', { token });
        } else {
          // Login failed, display error message or perform desired actions
          console.log('Login failed');
        }
      })
      .catch((error) => {
        console.log('Error:', error);
      });
  };

  return (
    <div className="container">
      <div className="row justify-content-center mt-4">
        <div className="col-lg-4 col-md-6">
          <h2 className="text-center mb-4">Login Page</h2>
          <form onSubmit={handleSubmit}>
            <div className="mb-3">
              <label className="form-label">Username:</label>
              <input type="text" className="form-control" value={username} onChange={handleUsernameChange} required />
            </div>
            <div className="mb-3">
              <label className="form-label">Password:</label>
              <input type="password" className="form-control" value={password} onChange={handlePasswordChange} required />
            </div>
            <div className="d-grid">
              <button type="submit" className="btn btn-primary">Login</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Login;
