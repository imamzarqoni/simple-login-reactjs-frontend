import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Login from './Login';
import UserInfo from './UserInfo';
import Pegawai from './Pegawai';
import Dashboard from './Dashboard';

const App = () => {
  return (
    <Router>
      <div>
        <h1>Your App</h1>
        <Routes>
          <Route path="/login" component={Login} />
          <Route path="/userinfo" render={({ location }) => {
            const { token } = location.state;
            return <UserInfo token={token} />;
          }} />
          <Route path="/pegawai" render={({ location }) => {
            const { token } = location.state;
            return <Pegawai token={token} />;
          }} />
          <Route path="/dashboard" render={({ location }) => {
            const { token } = location.state;
            return <Dashboard token={token} />;
          }} />
        </Routes>
      </div>
    </Router>
  );
};

export default App;
